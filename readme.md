# devNoteBook

i need to have a software based note book where data can be recured and may be shared with others,
+ create notes
+ Edit Notes
+ Search based on a ticket id
+ export in a human readable format (yaml)

## project
### dev notes
the working end of the python runtime is `runme.py`, to devop with this project you need to create a virutalenu that
uses python3.6, this can be achieved by using the command `virtualenv -p /usr/bin/python3.6 venu` you can then istall
the project decencies by using the command `pip install -r requirements.txt`.

### nameing convention
#### class methods
when a class method is defined it should ways return a `dict` (see exsample below), the exception to this rule are methods that are nameed with a preedeing under score; theses methods are internal methods to the class that are utills that are only needed for that class, they can just return passed data.
```python3
returnObj = {
  'success' [True || False],
  'dataObj': {}
}

```

the success key defines an action taken is success or has errored, and the dataObj is the place to put data.


### the software structure
there is software is structure thus
```
|- cli.py
|- notebook/
            |- __init__.py "business logic is layed here"
            |- ds.py "this is the data layer"
            |- email.py "this is the email runner"
```
