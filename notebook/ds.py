from datetime import datetime

from tinydb import TinyDB
from tinydb import Query


class DbRunnerBase:

    def __init__(self, filelocation='./db.json'):
        self.file = filelocation
        temp = datetime.now()
        datetemp = f'{temp.day}/{temp.month}/{temp.year}'
        self.dateStamp = f'{datetemp}|{temp.hour}-{temp.minute}:{temp.second}'
        pass


class DBbase(DbRunnerBase):

    def get_all_notes(self):

        db = TinyDB(self.file)
        result = db.all()
        db.close()

        try:
            return {
                'success': True,
                'dataObj': {
                    'data': result
                }
            }
        except Exception as err:
            return {
                'success': False,
                'dataObj': {
                    'error': err
                }
            }

    def get_ticket_notes(self, _ticketid: str):
        """

        :param _ticketid:
        :return: dict
        """
        try:
            q = Query()
            db = TinyDB(self.file)
            result = db.search(q.ticket_id == _ticketid)
            db.close()

            return {
                'success': True,
                'dataObj': {
                    'result': result
                }
            }
        except Exception as err:
            return {
                'success': False,
                'dataObj': {
                    'error': err
                }
            }
        pass

    def insert_new_note(self, ticket_id, ticket_note, status):
        """

        :param ticket_id:
        :param ticket_note:
        :param status:
        :return:
        """

        try:
            db = TinyDB(self.file)

            db.insert({
                'ticket_id': ticket_id,
                'ticket_note': ticket_note,
                'status': status,
                'timestamp': self.dateStamp
            })

            db.close()

            return {
                'success': True,
                'dataObj': {
                    'msg': f'ticket note added for ticket id - {ticket_id}'
                }
            }

        except Exception as err:

            return {
                'success': False,
                'dataObj': {
                    'error': err
                }
            }
