import smtplib
from email.message import EmailMessage

import base64


class Email:

    def __init__(
            self,
            email_server_addr: str,
            port: int,
            username: str,
            password: str,
            from_addr: str="dev@devNotesBook.org"
    ):
        """

        :param email_server_addr:
        :param port:
        :param username:
        :param password:
        :param from_addr:
        """
        self.addr = email_server_addr
        self.port = port
        self.user = {
            'uname': username,
            'pword': self._encripted_string(password),
            'email': from_addr
        }
        pass

    def _encripted_string(self, string):
        return base64.b64encode(bytes(string, 'utf-8'))

    def _decript_string(self, string):
        return str(base64.b64decode(string))[2:-1]

    def send(self, subject, message, to):

        try:
            # login
            # TODO; check on how to define a subject

            msgObject = EmailMessage()
            msgObject.set_content(message)
            msgObject['Subject'] = subject
            msgObject['From'] = self.user['email']
            msgObject['To'] = to

            emailObj = smtplib.SMTP(self.addr)
            emailObj.login(self.user['uname'], self.user['pword'])
            emailObj.send_message(msgObject)
            emailObj.quit()
            pass

            return {
                'success': True,
                'dataObj': {
                    'msg': 'message has been sent'
                }
            }
        except Exception as err:
            return {
                'success': False,
                'dataObj': {
                    'error': err
                }
            }
            pass
