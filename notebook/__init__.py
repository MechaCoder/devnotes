from notebook.ds import DBbase

# TODO: Email class
# TODO: a ymal class to run user setings and export
# TODO: browser ui
# TODO: TKinter
# TODO: logging


class NotesCli:

    def __init__(self, fileLocation: str='db.json'):
        self.file = fileLocation

    def get_notes_index(self, amount: int=10):
        """

        :param amount: int
        :return:
        """

        try:
            db = DBbase(self.file).get_all_notes()
            if db['success']:
                result = db['dataObj']['data'][0:amount]
                pass
            else:
                raise "db function error"

            return {
                'success': True,
                'dataObj': {
                    'data': result
                }
            }
        except Exception as err:
            return {
                'success': False,
                'dataObj': {
                    'error': err
                }
            }

    def insert_new_ticket(self, ticket_id, ticket_note, status):

        try:
            db = DBbase(self.file).insert_new_note(
                ticket_id,
                ticket_note,
                status
            )
            if db['success']:
                result = db['dataObj']['msg']
            else:
                raise "db function error"
            return {
                'success': True,
                'dataObj': {
                    'msg': result
                }
            }
        except Exception as err:
            return {
                'success': False,
                'dataObj': {
                    'error': err
                }
            }
