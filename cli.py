from notebook import NotesCli
from tabulate import tabulate
import click


@click.group()
@click.version_option()
def cli():
    click.echo('')
    pass


@cli.command('index', help="gets an index of notes")
@click.option('-on', type=int, default=10)
def index(on):
    notes = NotesCli()
    noteArray = notes.get_notes_index(on)
    click.echo(noteArray)
    valueArray = []
    for e in noteArray['dataObj']['data']:
        valueArray.append(
            [
                e['ticket_id'],
                e['status'],
                e['timestamp']
            ]
        )
    header = ['ticket id', 'status', 'timeStamp']
    click.echo(
        tabulate(valueArray, headers=header)
    )


@cli.command('add', help="adds a new note")
@click.option('-id', type=str, default='')
@click.option('-note', type=str, default='')
@click.option('-status', type=str, default='')
def add(id, note, status):

    while len(id) == 0:
        id = click.prompt('enter the id of ticket you are working on :')

    while len(note) == 0:
        note = click.prompt('enter all notes to be stored \n > ')

    while len(status) == 0:
        status = click.prompt('enter status')

    # click.echo(f'{id}, {note}, {status}')
    notesobj = NotesCli()
    result = notesobj.insert_new_ticket(id, note, status)
    if result['success']:
        click.echo('new note added')
    pass


if __name__ == '__main__':
    cli()
