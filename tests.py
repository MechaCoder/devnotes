from random import randint


import unittest
from os import remove

# import notebook.ds as notebook
import notebook.email as my_email
import notebook


class Base(unittest.TestCase):

    def setUp(self):
        self.dataBase = "./testing_db.json"
        self.dbObject = notebook.DBbase(self.dataBase)

    def exsam_object(self, _obj):
        """

        :param _obj: the object test
        :return:
        """

        self.assertEqual(
            type(_obj),
            type({}),
        )

        self.assertEqual(
            list(_obj.keys()),
            ['success', 'dataObj']
        )

        self.assertEqual(
            _obj['success'],
            True,
            msg="dose the return object have a success of True"
        )

        self.assertEqual(
            type(_obj['dataObj']),
            type({})
        )

    def tearDown(self):
        remove(self.dataBase)
        pass


class TestDatabase(Base):

    def test_db_insert_new_note(self):
        inserted = self.dbObject.insert_new_note(
            'ticketid',
            'ticket note',
            'todo'
        )
        self.exsam_object(inserted)

        pass

    def test_db_getAllNotes(self):
        notes = self.dbObject.get_all_notes()
        self.exsam_object(notes)
        pass

    def test_db_get_ticket_notes(self):
        self.dbObject.insert_new_note(
            'ticket 123456789',
            'this is a test',
            'status'
        )

        test = self.dbObject.get_ticket_notes('ticket 123456789')
        self.exsam_object(test)

        self.assertEqual(
            test['success'],
            True
        )

        self.assertEqual(
            type(test['dataObj']),
            type({})
        )

        self.assertEqual(
            list(test['dataObj'].keys()),
            ['result']
        )

        self.assertEqual(
            len(test['dataObj']['result']),
            1
        )

        pass


class Test_workingend(Base):

    def test_NoteCli_get_notes_index(self):
        notesCli = notebook.NotesCli(self.dataBase)

        obj = notesCli.get_notes_index()

        self.exsam_object(obj)

        self.assertEqual(
            type(obj['dataObj']['data']),
            type([])
        )

        self.assertTrue(
            len(obj['dataObj']['data']) <= 10
        )

    def test_NoteCli_insert_new_ticket(self):
        notecli = notebook.NotesCli(self.dataBase)
        notecli.insert_new_ticket(
            'ticketId',
            'test note ',
            'done'
        )

        # self.exsam_object(obj)

        pass


class Test_email(Base):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_init(self):
        testObj = {
            'server': "email{}".format(randint(0, 1000)),
            'port': randint(10, 100),
            'user': 'test user',
            'pw': 'passwordTest'
        }

        emailobj = my_email.Email(
            testObj['server'],
            testObj['port'],
            testObj['user'],
            testObj['pw']
        )

        self.assertTrue(
            emailobj.addr,
            testObj['server']
        )


if __name__ == '__main__':
    unittest.main()
